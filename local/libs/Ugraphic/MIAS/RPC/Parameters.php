<?

namespace Ugraphic\MIAS\RPC;

class Parameters 
{
    private static $systemId;
    private static $soapParams;

    private static $debugMode = false;

    /**
     * @return mixed
     */
    public static function getSystemId()
    {
        return self::$systemId;
    }

    /**
     * @param mixed $systemId
     */
    public static function setSystemId($systemId)
    {
        self::$systemId = $systemId;
    }

    /**
     * @return array
     */
    public static function getSoapParams()
    {
        return self::$soapParams;
    }

    /**
     * @param array $soapParams
     */
    public static function setSoapParams($soapParams)
    {
        self::$soapParams = $soapParams;
    }

    /**
     * @return mixed
     */
    public static function getDebugMode()
    {
        return self::$debugMode;
    }

    /**
     * @param boolean $debugMode
     */
    public static function setDebugMode($debugMode = false)
    {
        self::$debugMode = $debugMode;
    }
}