<?php

namespace Ugraphic\MIAS\RPC;

use Bitrix\Main\SystemException;
use Ugraphic\Api\Request,
    Ugraphic\MIAS\RPC;

abstract class SoapAPIController
{
    public $arResult;
    public $arRequest;

    public function setParams($arParams)
    {
        $this->arResult = $this->getRequest();
        $this->arRequest = $this->arResult["PARAMETERS"];
        $this->arResult["response"] = [];

        // Выполняем настройку SOAP клиента
        RPC\Parameters::setSystemId($arParams["systemId"]);
        RPC\Parameters::setSoapParams($arParams["SoapParams"]);
        RPC\Parameters::setDebugMode(false);

    }

    /**
     * Подготавливает массив с данными для ответа
     *
     * @param $soapResponse
     * @param $arParams
     * @return array
     */
    private function prepareResponse($soapResponse, $arParams)
    {
        if (!empty($soapResponse["error"]["code"])) {
            $code = $soapResponse["error"]["code"];
            $message = "Неизвестная ошибка";
        } else {
            $message = !empty($soapResponse["response_block"]["errorMessage"]) ? $soapResponse["response_block"]["errorMessage"] : $soapResponse["body"]["error"];
            $code = $soapResponse["response_block"]["result"];
        }

        $arResult = [
            "soap" => $soapResponse,
            "platform" => [
                "accountNumber" => $arParams["accountNumber"],
                "error" => [
                    "code" => $code,
                    "message" => $message
                ]
            ]
        ];

        return $arResult;
    }

    public function getMap()
    {
        return array();
    }

    // Get current request
    private function getRequest()
    {
        return Request::get();
    }


    public function getBalance()
    {

        $arParams = RPC\Helper::prepareRequestParams(
            [
                "requestData" => $this->arRequest,
                "paramsRequired" => [
                    "accountNumber",
                    "dateBalance"
                ]
            ]
        );

        $soapResponse = RPC\Controller\SoapController::getBalance($arParams);
        $arResult = self::prepareResponse($soapResponse, $arParams);

        return $arResult;
    }

    public function getFullBalance()
    {

        $arParams = RPC\Helper::prepareRequestParams(
            [
                "requestData" => $this->arRequest,
                "paramsRequired" => [
                    "accountNumber",
                    "dateBalance"
                ]
            ]
        );

        $soapResponse = RPC\Controller\SoapController::getFullBalance($arParams);
        $arResult = self::prepareResponse($soapResponse, $arParams);

        return $arResult;
    }


    /**
     * Передача показаний
     */
    public function addIndications()
    {
        $arParams = RPC\Helper::prepareRequestParams(
            [
                "requestData" => $this->arRequest,
                "paramsRequired" => [
                    "accountNumber",
                    "dateBalance",
                    "device"
                ]
            ]
        );

        // Передача показаний
        $soapResponse = RPC\Controller\SoapController::putListDevices($arParams);

        $arResult = self::prepareResponse($soapResponse, $arParams);

        if ($soapResponse["response_block"]["result"] != '0') {
            // Костяль для обработки исключения 1С
            if (strpos($soapResponse["response_block"]["result"], 'Запись с такими ключевыми полями существует!') !== false) {
                $arResult["platform"]["error"]["code"] = "-505";
                $arResult["platform"]["error"]["message"] = "Сегодня показания счетчиков по данному лицевому счету внести не удастся: они уже были внесены ранее.";
            }
        }

        return $arResult;

    }

    /**
     * Получание списка приборов учета и их показаний
     */
    public function getIndications()
    {
        $arParams = RPC\Helper::prepareRequestParams(
            [
                "requestData" => $this->arRequest,
                "paramsRequired" => [
                    "accountNumber",
                    "dateBalance"
                ]
            ]
        );

        $soapResponse = RPC\Controller\SoapController::getListDevices($arParams);

        $arResult = self::prepareResponse($soapResponse, $arParams);

        return $arResult;

    }


    /**
     * Возвращает URL на сайте банка для оплаты по карте.
     * В параметрах указывается номер Л/С и сумма платежа
     *
     * @return array
     */
    public function getPaymentUrl()
    {
        $arParams = RPC\Helper::prepareRequestParams(
            [
                "requestData" => $this->arRequest,
                "paramsRequired" => [
                    "accountNumber",
                    "paymentSum"
                ]
            ]
        );

        try {
            $soapResponse = RPC\Controller\PaymentController::getPaymentUrl($arParams);
        } catch (SystemException $e) {
        } catch (\Exception $e) {
        }

        $arResult = self::prepareResponse($soapResponse, $arParams);

        return $arResult;
    }

    /**
     * Список оплат по лицевому счёту
     *
     * @return array
     */
    public function getPayment()
    {
        $arParams = RPC\Helper::prepareRequestParams(
            [
                "requestData" => $this->arRequest,
                "paramsRequired" => [
                    "accountNumber",
                    "dateBegin",
                    "dateEnd"
                ]
            ]
        );

        $soapResponse = RPC\Controller\SoapController::getPayment($arParams);

        $arResult = self::prepareResponse($soapResponse, $arParams);

        return $arResult;
    }

}