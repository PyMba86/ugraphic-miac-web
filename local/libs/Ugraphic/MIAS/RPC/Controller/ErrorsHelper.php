<?

namespace Ugraphic\MIAS\RPC\Controller;


class ErrorsHelper
{

    /**
     * Возвращает текст ошибки по её коду для SOAP запроса.
     * Содержит ошибки, общие для всех запросов
     *
     * @param string $errorCode
     * @param array $arCustomError
     * @return string
     */
    public static function getBasicError(string $errorCode, array $arCustomError = [])
    {
        $arErrorCodes = [
            "-100" => "Система не найдена",
            "-101" => "Найдено несколько систем",
            "-200" => "Не найден счет или адрес",
            "-201" => "Найдено несколько счетов или адресов",
            "-202" => "Не соответствие адреса и счета",
            "-999" => "Общая ошибка исполнения запроса"
        ];

        if (!empty($arCustomError)){
            $arErrorCodes = $arErrorCodes + $arCustomError;
        }

        $result = !empty($arErrorCodes[$errorCode]) ? $arErrorCodes[$errorCode] : $errorCode;

        return $result;
    }

    /**
     * Возвращает текст ошибки по её коду для SOAP запроса GET_BALANCE
     * @param $errorCode
     */
    public static function getBalanceError($errorCode)
    {
        return self::getBasicError($errorCode);
    }

    /**
     * Возвращает текст ошибки по её коду для SOAP запроса GET_LIST_DEVICES
     * @param $errorCode
     */
    public static function getListDeviceError($errorCode)
    {
        return self::getBasicError($errorCode);
    }

    /**
     * Возвращает текст ошибки по её коду для SOAP запроса PUT_LIST_DEVICES
     * @param $errorCode
     */
    public static function putListDevicesError($errorCode)
    {
        $arCustomError = [
            "-502" => "Слишком большая разница в показаниях",
        ];

        return self::getBasicError($errorCode, $arCustomError);
    }

}