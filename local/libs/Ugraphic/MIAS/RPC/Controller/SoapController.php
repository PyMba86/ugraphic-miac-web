<?

namespace Ugraphic\MIAS\RPC\Controller;

use Ugraphic\MIAS\RPC\Parameters,
    Ugraphic\MIAS\RPC\Helper;

\Bitrix\Main\Loader::includeModule("webservice");

class SoapController
{

    public static function getBalance($arParams)
    {
        $arParams["dateBalance"] = (empty($arParams["dateBalance"])) ?
            date("Y-m-d", strtotime("today")) :
            $arParams["dateBalance"];

        $arParameters = array(
            "name" => "request",
            "value" => array(
                "request_block" => array(
                    "system_id" => Parameters::getSystemId(),
                    "session_id" => bitrix_sessid()
                ),
                "body" => array(
                    "date_balance" => $arParams["dateBalance"],
                    "account" => array("number" => $arParams["accountNumber"]),
                    "address" => array(
                        "street" => $arParams["street"],
                        "building" => $arParams["building"],
                        "korpus" => "",
                        "flat" => $arParams["flat"],
                        "litera" => $arParams["litera"]
                    ),
                    "GUID_provider" => $arParams["rsoGuid"],
                )

            )
        );


        $arResult = Helper::sendSoapRequest("GET_BALANCE", "IRC_BILLING", $arParameters);

        if ($arResult["response_block"]["result"] != 0) {
            $arResult["response_block"]["errorMessage"] = ErrorsHelper::getBalanceError($arResult["response_block"]["result"]);
        }

        return $arResult;
    }

    /**
     * Получение развернутой таблицы с начислениями
     *
     * @param $arParams
     * @return array|bool
     */
    public static function getFullBalance($arParams)
    {
        $arParams["dateBalance"] = (empty($arParams["dateBalance"])) ?
            date("Y-m-d", strtotime("today")) :
            $arParams["dateBalance"];

        $arParameters = array(
            "name" => "request",
            "value" => array(
                "request_block" => array(
                    "system_id" => Parameters::getSystemId(),
                    "session_id" => bitrix_sessid()
                ),
                "body" => array(
                    "date_balance" => $arParams["dateBalance"],
                    "account" => array("number" => $arParams["accountNumber"]),
                    "address" => array(
                        "street" => $arParams["street"],
                        "building" => $arParams["building"],
                        "korpus" => "",
                        "flat" => $arParams["flat"],
                        "litera" => $arParams["litera"]
                    ),
                    "GUID_provider" => $arParams["rsoGuid"],
                )

            )
        );


        $arResult = Helper::sendSoapRequest("GET_FULL_BALANCE", "IRC_BILLING", $arParameters);

        if ($arResult["response_block"]["result"] != 0) {
            $arResult["response_block"]["errorMessage"] = ErrorsHelper::getBalanceError($arResult["response_block"]["result"]);
        }

        return $arResult;
    }

    /**
     * Возвращает показания счетчиков по указанному Л/С на указанную дату
     * @param $arParams
     *      ["accountNumber"]
     *      ["dateBalance"]
     * @return array
     */
    public static function getListDevices($arParams)
    {
        $arParameters = array(
            "name" => "request",
            "value" => array(
                "request_block" => array(
                    "system_id" => Parameters::getSystemId(),
                    "session_id" => bitrix_sessid()
                ),
                "body" => array(
                    "date_balance" => $arParams["dateBalance"],
                    "account" => array("number" => $arParams["accountNumber"]),
                    "address" => array(
                        "street" => "",
                        "building" => "",
                        "korpus" => "",
                        "flat" => "",
                        "litera" => ""
                    )
                )
            )
        );

        $arResult = Helper::sendSoapRequest("GET_LIST_DEVICES", "IRC_BILLING", $arParameters);

        if ($arResult["response_block"]["result"] != 0) {
            $arResult["response_block"]["errorMessage"] = ErrorsHelper::getBalanceError($arResult["response_block"]["result"]);
        }

        return $arResult;
    }

    /**
     * Передает показания счетчиков по указанному Л/С на указанную дату
     * @param $arParams
     *      string ["accountNumber"]
     *      array  ["dateBalance"]
     * @return array
     */
    public static function putListDevices($arParams)
    {
        $arDevices = "";
        foreach ($arParams["device"] as $PutDeviceItem) {
            if ($PutDeviceItem["value"] >= 0.01) {
                $arDevices .= (string)'<device><device_id>' . $PutDeviceItem["id"] . '</device_id><num>' . $PutDeviceItem["value"] . '</num></device>';
            }
        }

        $arParameters = array(
            "name" => "request",
            "value" => array(
                "request_block" => array(
                    "system_id" => Parameters::getSystemId(),
                    "session_id" => bitrix_sessid()
                ),
                "body" => array(
                    "account" => array("number" => $arParams["accountNumber"]),
                    "address" => array(
                        "street" => "",
                        "building" => "",
                        "korpus" => "",
                        "flat" => "",
                        "litera" => ""
                    ),
                    "devices" => $arDevices
                )
            )
        );

        $arResult = Helper::sendSoapRequest("PUT_LIST_DEVICES", "IRC_BILLING", $arParameters);

        if ($arResult["response_block"]["result"] != 0) {
            $arResult["response_block"]["errorMessage"] = ErrorsHelper::putListDevicesError($arResult["response_block"]["result"]);
        }

        return $arResult;
    }

    /**
     * Получение квитанции
     * @param $arParams
     *      string ["accountNumber"]
     *      array  ["dateBalance"]
     * @return array
     */
    public static function getKvit($arParams)
    {

        $arParameters = array(
            "name" => "request",
            "value" => array(
                "request_block" => array(
                    "system_id" => Parameters::getSystemId(),
                    "session_id" => bitrix_sessid()
                ),
                "body" => array(
                    "date_balance" => $arParams["dateBalance"],
                    "account" => array("number" => $arParams["accountNumber"])
                )

            )
        );

        $arResult = Helper::sendSoapRequest("GET_KVIT", "IRC_BILLING", $arParameters);

        if ($arResult["response_block"]["result"] != 0) {
            $arResult["response_block"]["errorMessage"] = ErrorsHelper::getBasicError($arResult["response_block"]["result"]);
        }

        return $arResult;
    }

    /**
     * Создает платежный документ в 1С
     * @param $arParams
     * [accountNumber]
     * [sumPay]
     * [FIO]
     * [rsoGuid]
     * [rsoId]
     * [body]
     * [HLElementID]
     *
     * @return array
     */
    public static function sendPayment($arParams)
    {

        $arBalance = $arParams["body"];


        $service_sum_list = "";
        $SUM_PAYMENT_SOAP = (float)$arParams["sumPay"];

        foreach ($arBalance["address_service"]["account_service_list"]["account_service"]["service_sum_list"] as $arServiceItem) {
            $service_sum_list .= (string)'<service_sum>' .
                '<service_id>' . $arServiceItem["service_id"] . '</service_id>' .
                '<service_name>' . $arServiceItem["service_name"] . '</service_name>' .
                '<measure_unit/>' .
                '<sum_begin/>' .
                '<sum>' . $SUM_PAYMENT_SOAP . '</sum>' .
                '<sum_peny_begin/>' .
                '<sum_peny>0.00</sum_peny>' .
                '<tarif_sum/>' .
                '<sum_accrual/>' .
                '<sum_payment/>' .
                '<sum_imprest>0.00</sum_imprest>' .
                '<sum_peny_accrual/>' .
                '<sum_peny_payment/>' .
                '</service_sum>';
            $SUM_PAYMENT_SOAP = '0.00';
        }

        $arParameters = array(
            "name" => "request",
            "value" => array(
                "request_block" => array(
                    "system_id" => Parameters::getSystemId(),
                    "session_id" => bitrix_sessid()
                ),
                "body" => array(
                    "account_service" => array(
                        "number" => $arParams["accountNumber"],
                        "service_sum_list" => $service_sum_list,
                        "date_open" => $arBalance["address_service"]["account_service_list"]["account_service"]["date_open"],
                    ),
                    "payment_method" => '31135ad0-201e-11e7-6898-000c29e30103',
                    "num_payment" => $arParams["HLElementID"],
                    "distr_method" => '5010f5d2-07f3-4ad7-b019-469dc0586710'
                )
            )
        );

        $arResult = Helper::sendSoapRequest("PAYMENT", "IRC_BILLING", $arParameters);

        if ($arResult["response_block"]["result"] != 0) {
            $arResult["response_block"]["errorMessage"] = ErrorsHelper::getBasicError($arResult["response_block"]["result"]);
        }

        return $arResult;
    }

    /**
     * Создает платежный документ в 1С
     * @param $arParams
     * [documentId]
     *
     * @return array
     */
    public static function postDocument($arParams)
    {
        $arParameters = array(
            "name" => "request",
            "value" => array(
                "request_block" => array(
                    "system_id" => Parameters::getSystemId(),
                    "session_id" => bitrix_sessid()
                ),
                "body" => array(
                    "document_id" => $arParams["documentId"]
                )
            )
        );

        $arResult = Helper::sendSoapRequest("POST_DOCUMENT", "IRC_BILLING", $arParameters);

        if ($arResult["response_block"]["result"] != 0) {
            $arResult["response_block"]["errorMessage"] = ErrorsHelper::getBasicError($arResult["response_block"]["result"]);
        }

        return $arResult;
    }

    /**
     * Список оплат по лицевому счёту
     *
     * @param $arParams
     * @return array
     */
    public static function getPayment($arParams)
    {
        $arParameters = array(
            "name" => "request",
            "value" => array(
                "request_block" => array(
                    "system_id" => Parameters::getSystemId(),
                    "session_id" => bitrix_sessid()
                ),
                "body" => array(
                    "date_begin" => $arParams["dateBegin"],
                    "date_end" => $arParams["dateEnd"],
                    "account" => array("number" => $arParams["accountNumber"]),
                    "address" => array(
                        "street" => $arParams["street"],
                        "building" => $arParams["building"],
                        "korpus" => "",
                        "flat" => $arParams["flat"],
                        "litera" => $arParams["litera"]
                    ),
                )
            )
        );

        $arResult = Helper::sendSoapRequest("GET_PAYMENT", "IRC_BILLING", $arParameters);

        if ($arResult["response_block"]["result"] != 0) {
            $arResult["response_block"]["errorMessage"] = ErrorsHelper::getBasicError($arResult["response_block"]["result"]);
        }

        return $arResult;

    }

}