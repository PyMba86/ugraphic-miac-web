<?

namespace Ugraphic\MIAS\RPC;

use \Bitrix\Main\Application,
    \Bitrix\Main\Web\Cookie;


class Helper
{

    /**
     * Подготовка параметров
     *
     * @param array $arRequest
     *  $arRequest = [
     *      "paramsRequired",
     *      "requestData"
     * ]
     *
     * @return array
     */
    public static function prepareRequestParams($arRequest = [])
    {
        $arResult = [];

        foreach ($arRequest["paramsRequired"] as $key) {
            $requestItem = $arRequest["requestData"][$key];

            switch ($key) {
                case "dateBalance": // Если запрошена дата, но не передано значение - ставим текущую дату
                    $arResult["dateBalance"] = empty($requestItem) ? date("Y-m-d", strtotime("today")) : $requestItem;
                    break;
                case "dateBegin":
                    $arResult["dateBegin"] = empty($requestItem) ? date("Y-m-d", strtotime("today")) : $requestItem;
                    break;
                case "dateEnd":
                    $arResult["dateEnd"] = empty($requestItem) ? date("Y-m-d", strtotime("today")) : $requestItem;
                    break;

                case "rsoGuid":
                    $arRso = self::getRsoGuid();
                    $arResult["rsoGuid"] = $arRso["GUID"];
                    $arResult["rsoId"] = $arRso["ID"];
                    break;

                case "device":
                    $arResult["device"] = $requestItem;
                    break;

                default:
                    $arResult[$key] = htmlspecialcharsbx($requestItem);
            }
        }

        return $arResult;
    }


    /**
     * Статический метод. Отправляет SOAP запрос с указанными параметрами
     *
     * @param string $Method
     * @param string $Namespace
     * @param array $arQueryParameters
     * @param bool $manualDebug
     *
     * @return bool | array
     *
     * @static
     */
    public static function sendSoapRequest($Method = "", $Namespace = "IRC_BILLING", $arQueryParameters = array(), $manualDebug = false)
    {
        \Bitrix\Main\Loader::includeModule("webservice");

        $arSoapParams = Parameters::getSoapParams();

        $client = new \CSOAPClient($arSoapParams["host"], $arSoapParams["path"]);

        $client->setLogin($arSoapParams["login"]);
        $client->setPassword($arSoapParams["password"]);
        $client->setTimeout(10);

        $request = new \CSOAPRequest($Method, $Namespace);

        $request->addParameter($arQueryParameters["name"], $arQueryParameters["value"]);

        $response = $client->send($request);

        if ($response == 0) {
            return [
                "error" => [
                    "type" => "Connection fault",
                    "code" => "-1",
                    "errorNumber" => $client->errorNumber,
                    "errorString" => $client->errorString,
                    "message" => $client->ErrorString
                ]
            ];
        }

        if ($response->isFault()) {
            return [
                "error" => [
                    "type" => "SOAP fault",
                    "code" => "-1",
                    "errorNumber" => $response->faultCode(),
                    "errorString" => $response->faultString()
                ]
            ];
        }

        return $response->Value;
    }
}