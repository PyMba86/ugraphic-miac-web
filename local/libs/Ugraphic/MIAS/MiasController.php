<?php

namespace Ugraphic\MIAS;

use Ugraphic\Api\Response,
    Ugraphic\MIAS\RPC;

class MiasController extends RPC\SoapAPIController
{

    function __construct($arParams)
    {
        parent::setParams($arParams);
    }

    private function showJsonResult($funcName)
    {
        $this->arResult["response"] = parent::$funcName();
        Response::ShowResult($this->arResult);
    }

    public function getMap()
    {
        $arResult = [
            "getCities" => [
                "description" => "Получение списка направлений по ID учреждения",
                "params" => [
                    "accountNumber" => [
                        "required" => true,
                        "type" => "string",
                        "description" => "Номер Л/С"
                    ]
                ]
            ],
        ];

        $this->arResult["response"] = $arResult;

        Response::ShowResult($this->arResult);
    }


    /**
     * Пример получение ответа
     * @return array|void
     */
    public function getBalance()
    {
        $this->showJsonResult(__FUNCTION__);
    }

}