<?php

namespace Ugraphic\Api;


class Router extends Init
{
    private static $params;

    public function start()
    {
        // Get params
        switch (self::getMethod()) {
            case 'GET':
                $requestUri = str_replace('/?', '?', $_SERVER['REQUEST_URI']);
                if (strstr($requestUri, '?', false) !== false) {
                    $pathParts = explode('?', $requestUri);
                    $pathParts = ($pathParts[1]) ? explode('&', $pathParts[1]) : [];
                    if ($pathParts) {
                        $tmp = [];
                        foreach ($pathParts as $item) {
                            $item = explode('=', $item);
                            $tmp[urldecode($item[0])] = urldecode($item[1]);
                        }
                        $pathParts = $tmp;
                    }
                }
                break;
            case 'POST':
                $pathParts = ($_SERVER['CONTENT_TYPE'] == 'application/json') ? json_decode(file_get_contents('php://input'), true) : $_POST;
                break;
            case 'PUT':
                if ($_SERVER['CONTENT_TYPE'] == 'application/json') {
                    $pathParts = json_decode(file_get_contents('php://input'), true);
                }
                break;
            case 'DELETE':
                if ($_SERVER['CONTENT_TYPE'] == 'application/json') {
                    $pathParts = json_decode(file_get_contents('php://input'), true);
                }
                break;
            case 'OPTIONS':
                if ($_SERVER['CONTENT_TYPE'] == 'application/json') {
                    $pathParts = json_decode(file_get_contents('php://input'), true);
                }
                break;
        }
        self::$params = (count($pathParts) > 0) ? $pathParts : [];

        // Run controller
        if ($this->getController()) {
            if ($this->getAction()){
                $controller = new Controller();
                $controller->run();
            }
            else{
                Response::BadRequest("Action is empty", static::class);
            }
        } else {
            Response::BadRequest("Не передан контроллер", static::class);
        }
        die();
    }

    // ADDITIONAL METHODS

    public function getParameters()
    {
        return self::$params;
    }

    public function getAction()
    {
        $action = self::$params["action"];

        return !empty($action) ? $action : false;
    }

    public function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}