<?php

namespace Ugraphic\Api;


class Controller extends Router
{
    const ROOT_DIR_CONTROLLERS = 'controllers';
    const FILE = 'FILE';

    // MAIN METHODS


    public function run()
    {
        $this->startObjectOriented();
    }


    // ADDITIONAL METHODS
    // Start object-oriented
    private function startObjectOriented()
    {
        $controller = parent::getController();
        if (class_exists(parent::getController())) {
            $controllerObject = new $controller(parent::getControllerParams());

            if (method_exists($controllerObject, parent::getAction())) {
                // Start action
                $action = parent::getAction();
                $controllerObject->$action();

            } else {
                Response::BadRequest("Метод " . parent::getAction() . " не найден в контроллере " . parent::getController(), static::class);
            }

        } else {
            Response::BadRequest("Контроллер " . parent::getController() . " не найден", static::class);
        }
    }

}