<?php


namespace Ugraphic\Api;


class Request extends Router
{
    public static function get()
    {
        $arResult = [
            'DATE' => date('Y-m-d H:i:s'),
            'REQUEST_METHOD' => parent::getMethod(),
            'IP_ADDRESS' => parent::getRealIpAddr(),
            'CONTROLLER' => parent::getController(),
            'ACTION' => parent::getAction(),
            'PARAMETERS' => parent::getParameters()
        ];


        return $arResult;
    }
}