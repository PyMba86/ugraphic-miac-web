<?php

namespace Ugraphic\Api;


class Init
{
    public static $controller;
    public static $arControllerParams;
    public static $realIp;

    public function start($arParams = [])
    {
        $controller = $arParams["controller"];
        $this->setControllerParams($arParams["params"]);

        if (empty($controller)) {
            Response::BadRequest("Bad request. Контроллер не указан", static::class);
        } else {
            $this->setController($controller);
        }

        $this->setHeaders();

        // Run router/dispatch
        $router = new Router();
        $router->start();
    }

    private function setController($controller)
    {
        self::$controller = $controller;
    }

    public function getController()
    {
        return self::$controller;
    }

    private function setControllerParams($arParams)
    {
        self::$arControllerParams = $arParams;
    }

    public function getControllerParams()
    {
        return self::$arControllerParams;
    }

    // HEADERS

    private function setHeaders()
    {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, Authorization-Token');
        // Cross domain
        header('Access-Control-Allow-Origin: ' . $_SERVER['SERVER_NAME']);

    }

    public function getRealIpAddr()
    {
        if (!self::$realIp) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                self::$realIp = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                self::$realIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                self::$realIp = $_SERVER['REMOTE_ADDR'];
            }
        }
        return self::$realIp;
    }

}